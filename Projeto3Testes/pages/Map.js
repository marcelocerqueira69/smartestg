import React, { Component, useRef, createRef, useState } from 'react';
import MapView, { PROVIDER_GOOGLE, Marker, Callout, Polygon } from 'react-native-maps';
import { StyleSheet, View, Text, TouchableOpacity, Image, Alert, Dimensions, Button } from 'react-native';
import axios from 'axios';
import Geolocation from '@react-native-community/geolocation';
import { request, PERMISSIONS } from 'react-native-permissions';
import firestore, { firebase } from '@react-native-firebase/firestore';
//import Modal from "react-native-modal";
import { Modalize } from "react-native-modalize";
import database from '@react-native-firebase/database';

const { width: WIDTH } = Dimensions.get('window');


class Map extends Component {

  constructor(props) {
    super(props);
    this.modalizeRef = React.createRef();
    //this.modalizeRef = null;
    this.onOpenModal = this.onOpenModal.bind(this);
    this.forceUpdateHandler = this.forceUpdateHandler.bind(this);

    this.state = {
      navigation: props.navigation,
      polyList: [],
      userLogged: props.user.userLogged,
      activePoly: null,
      idFila: '',
      numFila: '',
      user: '',
      full: '',
      count: '',
      usersConf: {},
      stemp: '',
      Time: '',
      finalTime: '',
      initialTimeString: '',
      timeString: '',
      osUsersConf: {},
      poligono: {},
      totalConfs: 0
    }

  }


  shouldComponentUpdate(nextProps) {
    if (this.props.fila == undefined) {
      return false;
    } else {

      if (nextProps.fila != this.props.fila) {
        this.updateData(nextProps)
        return true;
      } else {
        return false;
      }
    }
  }

  watchID /*: ?number*/ = null;
  async componentDidMount() {
    this.props.setTitle('Mapa')
    this.requestLocalPermission();
    this.fetchData();
    this._map.setMapBoundaries({ latitude: 41.695, longitude: -8.844 }, { latitude: 41.693, longitude: -8.848 });

    this.willFocus = this.state.navigation.addListener(
      'focus', () => {
        console.log("MUAHAHAHAHAHH EU ENTREI");
        this.fetchData();
        this.props.setTitle('Mapa')
      }
    )
  }

  componentWillUnmount() {
    this.willFocus();
    Geolocation.clearWatch(this.watchID);
  }

  async updateData(props) {
    let fila = props.fila;

    var filaCor = await this.changeOneFilaColor(fila);

    var novoArray = this.state.polyList;
    novoArray[fila.num - 1] = filaCor;

    this.setState({ polyList: novoArray });

    this.forceUpdate()
  }


  async fetchData() {
    await database()
      .ref('/filas/')
      .once('value')
      .then(async snapshot => {
        let filas = snapshot.val();
        filas.shift();

        var listaPoligonos = await this.changeColors(filas);
        this.setState({ polyList: listaPoligonos });

        this.forceUpdate()
      });

  }

  requestLocalPermission = async () => {
    var response = await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
    console.log('permission: ' + response);

    if (response === 'granted') {
      this.currentPosition();
    }
  }

  currentPosition = () => {
    Geolocation.getCurrentPosition(
      position => {
        let initialPosition = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.0025,
          longitudeDelta: 0.0025
        }

        this.setState({ initialPosition });
      },
      error => Alert.alert(error.message),
      { enableHighAccuracy: true, timeout: 10000 }
    )

    this.watchID = Geolocation.watchPosition(
      position => {
        this.setState(position);
        const lastPosition = JSON.stringify(position);
        this.setState({ lastPosition });
      },
      error => Alert.alert(error.message),
      { enableHighAccuracy: true, timeout: 10000, distanceFilter: 1 }
    )
  }

  changeOneFilaColor = async (fila) => {
    const colorChooser = (full, time) => {
      var opacity;

      switch (true) {
        case time < 15:
          opacity = 1;
          break;
        case time >= 15 && time < 30:
          opacity = 0.7;
          break;
        case time >= 30 && time < 45:
          opacity = 0.5;
          break;
        case time >= 45:
          opacity = 0.3
          break;
        default:
          opacity = 1;
          break;
      }

      return `rgba(${full ? "0, 220" : "220, 0"},0, ${opacity})`;
    }

    const calculateTimeStamp = (current, stamp) => {
      return ((current - stamp) / 60).toFixed(0);
    }

    let stempTimeStamp = fila.timestamp / 1000
    let currTimeStamp = Date.now() / 1000;

    let time = calculateTimeStamp(currTimeStamp, stempTimeStamp);

    return { ...fila, cor: colorChooser(fila.full, time) }
  }


  changeColors = async (filas) => {


    const colorChooser = (full, time) => {
      var opacity;

      switch (true) {
        case time < 15:
          opacity = 1;
          break;
        case time >= 15 && time < 30:
          opacity = 0.7;
          break;
        case time >= 30 && time < 45:
          opacity = 0.5;
          break;
        case time >= 45:
          opacity = 0.3
          break;
        default:
          opacity = 1;
          break;
      }

      return `rgba(${full ? "0, 220" : "220, 0"},0, ${opacity})`;
    }

    const calculateTimeStamp = (current, stamp) => {
      return ((current - stamp) / 60).toFixed(0);
    }

    const newArray = filas.map((e, index) => {
      let stempTimeStamp = e.timestamp / 1000
      let currTimeStamp = Date.now() / 1000;

      let time = calculateTimeStamp(currTimeStamp, stempTimeStamp);

      return { ...e, cor: colorChooser(e.full, time) };
    })
    return newArray;

  }


  setStates(poligono) {
    this.setState({
      idFila: poligono.id,
      full: poligono.full,
      count: poligono.count,
      usersConf: Object.values(poligono.usersConf),
      numFila: poligono.num,
      user: poligono.user,
      osUsersConf: Object.values(poligono.usersConf),
      poligono: poligono,
      activePoly: poligono.num
    })
  }

  async onOpenModal(poligono) {
    await this.getFirebaseStamps(poligono);
    this.modalizeRef.current?.open();

    /*for (let i = 0; i < this.state.polyList.length; i++) {
      if (poligono.num === this.state.polyList[i].num) {
        this.setState({ activePoly: this.state.polyList[i].num });
      }
    }*/
  }

  toggleActiveColor(num) {
    return num == this.state.activePoly;
  }

  async forceUpdateHandler(string) {
    this.setState({ user: this.state.userLogged })
    if (string == 'false') {
      this.state.full ? this.setState({ full: false }) : this.setState({ full: true });
      this.state.count = 0;
    } else {
      this.state.count++;
    }

    if (this.state.count != 0) {
      this.state.osUsersConf[this.state.count] = this.state.user

      await database()
        .ref('/filas/' + this.state.numFila + '/usersConf/')
        .push(this.state.user)
        .then(console.log('user pushed'))

      await database()
        .ref('/users/' + this.state.osUsersConf[0] + '/atributos')
        .update({
          Confirmado: database.ServerValue.increment(1)
        })
        .then(console.log('incremented'))
        .catch(err => console.log(err))

      await database()
        .ref('/users/' + this.state.usersConf[0] + '/atributos/Confirmado')
        .once('value')
        .then(snapshot => {

          let snap = snapshot.val();


          switch (snap) {
            case 5:
              database()
                .ref('/users/' + this.state.osUsersConf[0] + '/medalhasGanhas')
                .push(8)
                .then(() =>
                  console.log("Medalha 5 confirmações added")
                );

              database()
                .ref('/users/' + this.state.osUsersConf[0])
                .update({
                  XP: database.ServerValue.increment(10),
                })
                .then(() => console.log('Xp dado ao primeiro.'));
              break;
            case 20:
              database()
                .ref('/users/' + this.state.osUsersConf[0] + '/medalhasGanhas')
                .push(9)
                .then(() =>
                  console.log("Medalha 20 confirmações added")
                );

              database()
                .ref('/users/' + this.state.osUsersConf[0])
                .update({
                  XP: database.ServerValue.increment(70),
                })
                .then(() => console.log('Xp dado ao primeiro.'));
              break;
            case 50:
              database()
                .ref('/users/' + this.state.osUsersConf[0] + '/medalhasGanhas')
                .push(10)
                .then(() =>
                  console.log("Medalha 50 confirmações added")
                );
              database()
                .ref('/users/' + this.state.osUsersConf[0])
                .update({
                  XP: database.ServerValue.increment(190),
                })
                .then(() => console.log('Xp dado ao primeiro.'));
              break;
            default:
              console.log("Nenhuma medalha de convite added");
              break;

          }
        })
      await database()
        .ref('/filas/' + this.state.numFila + '/')
        .update({
          user: this.state.user,
          full: this.state.full,
          count: database.ServerValue.increment(1),
          timestamp: database.ServerValue.TIMESTAMP
        })
        .then(() => { this.forceUpdate() })
        .catch(err => console.log(err));

    } else {
      database()
        .ref('/filas/' + this.state.numFila + '/')
        .update({
          user: this.state.user,
          full: this.state.full,
          count: 0,
          timestamp: database.ServerValue.TIMESTAMP
        })
        .then(() => {
          this.forceUpdate()
        })
        .catch(err => console.log(err));

      this.state.osUsersConf = {}

      this.state.osUsersConf[0] = this.state.user

      database()
        .ref('/filas/' + this.state.numFila + '/')
        .update({
          usersConf: this.state.osUsersConf
        })
        .then(console.log('meh'))
        .catch(err => console.log(err))
    }

    await database()
      .ref('/filas/' + this.state.numFila + '/usersConf')
      .once('value')
      .then(snapshot => {
        this.setState({
          usersConf: Object.values(snapshot.val())
        })
        console.log(this.state.usersConf)
      })
      .catch(err => console.log(err));

    this.setState({
      initialTimeString: '',
      timeString: 'agora mesmo',
      finalTime: this.state.time
    })
    this.setState({ activePoly: -1 });
    await this.getFirebaseStamps(this.state.poligono);
    this.fetchData();
    this.modalizeRef.current?.close();
  }

  async getFirebaseStamps(poligono) {
    this.setState({
      stemp: poligono.timestamp
    })
    if (this.state.stemp != '') {
      this.setState({
        Time: ((((Date.now()) - this.state.stemp) / 1000) / 60).toFixed(0)
      })
    } else {
      alert('clica again')
    }


    if (this.state.Time < 1) {
      this.setState({
        initialTimeString: '',
        timeString: 'agora mesmo',
        finalTime: this.state.time
      })
    } else if (this.state.Time > 60 && this.state.Time < 1440) {
      this.setState({
        initialTimeString: ' há ',
        timeString: 'h',
        finalTime: ((this.state.Time) / 60).toFixed(0)
      })
    } else if (this.state.Time >= 1440) {
      this.setState({
        initialTimeString: ' há ',
        timeString: 'd',
        finalTime: ((this.state.Time) / 1440).toFixed(0)
      })
    } else {
      this.setState({
        initialTimeString: ' há ',
        timeString: 'm',
        finalTime: this.state.Time
      })
    }
    this.forceUpdate()
  }



  render() {
    return (
      <View>
        <MapView
          provider={PROVIDER_GOOGLE}
          ref={map => this._map = map}
          showsUserLocation={true}
          mapType='satellite'
          style={styles.map}
          initialRegion={this.state.initialPosition}
        >

          {this.state.polyList.map(poligono => (<Polygon
            key={poligono.num}
            coordinates={poligono.coordinates}
            fillColor={poligono.cor}
            strokeColor={this.toggleActiveColor(poligono.num) ? "rgba(255, 255, 0, 1)" : "rgba(150, 150, 150, 0.8)"}
            tappable={true}
            onPress={() => { this.setStates(poligono); setTimeout(() => { this.onOpenModal(poligono); }, 500); }}
          >
          </Polygon>))}

        </MapView>

        <View style={{ backgroundColor: 'white' }}>
          <Text
            style={{ marginTop: '2%', marginLeft: '5%', fontWeight: 'bold' }}>Legenda de cores (ao minuto):</Text>
          <Image
            style={{ alignSelf: 'center', marginLeft: '3%' }}
            source={require('../images/legendaCores.png')}
          />
        </View>

        <Modalize ref={this.modalizeRef} snapPoint={320} onClose={() => { this.setState({ activePoly: -1 }); this.forceUpdate() }}>
          <View style={{ flex: 1, height: 300, flexDirection: 'column', justifyContent: 'center', marginTop: 50, marginLeft: 20, marginRight: 20 }}>
            <View style={styles.textView}>
              {this.state.full ?
                <Text style={styles.textLivre}>Esta fila está livre!</Text> :
                <Text style={styles.textFull}>Esta fila está ocupada...</Text>}


              <Text style={styles.textUser}>Ultima atualização feita por {this.state.user}{this.state.initialTimeString}{this.state.finalTime} {this.state.timeString}.</Text>
              <Text style={styles.textUser}>Número de confirmações: {this.state.count}</Text>
            </View>
            <View style={styles.buttonview}>
              <TouchableOpacity
                style={styles.Confirm}
                onPress={() => {
                  let exist = false;
                  for (var i = 0; i < this.state.usersConf.length; i++) {
                    if (this.state.usersConf[i] === this.state.userLogged) {
                      exist = true;
                    }
                  }

                  if (exist) {
                    Alert.alert(
                      'Falha',
                      'Não pode confirmar mais do que uma vez!',
                      [
                        { text: 'Ok' },
                      ]
                    )
                  } else {
                    Alert.alert(
                      'Confirmação',
                      'Obrigado pela sua confirmação!',
                      [
                        { text: 'Ok', onPress: () => this.forceUpdateHandler('true') },
                      ]
                    )
                  }

                }}>
                <Text style={styles.imageButton}>Confirmar</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.Correct}
                onPress={() => Alert.alert(
                  'Correção',
                  'De certeza que a informação está errada?',
                  [
                    { text: 'Não', onPress: () => console.log('cancelado') },
                    { text: 'Sim', onPress: () => this.forceUpdateHandler('false') },
                  ]
                )}>
                <Text style={styles.imageButton}>Corrigir</Text>
              </TouchableOpacity>
            </View>
          </View>

        </Modalize>

      </View>

    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 40
  },
  map: {
    height: '81%' //76
  },
  Logout: {
    alignItems: 'flex-end',
    marginRight: 40,
    bottom: "-1%"
  },
  imageButton: {
    height: 30,
    marginTop: 10
  },
  textView: {
    flex: 0.5,
    marginTop: '20%',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  full: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  },
  buttonview: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  textLivre: {
    alignContent: 'center',
    color: 'green',
    fontSize: 36
  },
  textFull: {
    color: 'red',
    fontSize: 36
  },
  textUser: {
    color: 'gray',
    fontSize: 18,
    marginTop: '1%',
    textAlign: 'center'
  },
  textView: {
    flex: 0.5,
    marginTop: '20%',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  buttonView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {
    width: WIDTH - 200,
    backgroundColor: '#69dbd4',
    marginTop: '30%'
  },
  Confirm: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    height: 30,
    width: 50,
    backgroundColor: '#46c75b',
    borderRadius: 5,
    margin: 10
  },
  Correct: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    height: 30,
    width: 50,
    backgroundColor: '#ff1919',
    borderRadius: 5,
    margin: 10
  },
})

export default Map;