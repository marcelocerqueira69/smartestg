import React, { Component } from 'react';
import * as tudo from 'react-native';
import axios from 'axios';
import auth from '@react-native-firebase/auth';
let user = auth().currentUser;

class Profile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            navigation: props.navigation,
            id: props.id,
            user: props.user,
            email: props.user.user.email,
            oldPassword: '',
            newPassword: '',
            newPasswordC: ''
        }
    }

    componentDidMount() {
        this.props.setTitle('Editar Password')

        this.willFocus = this.state.navigation.addListener(
            'focus', () => {
                this.props.setTitle('Editar Password')
            }
        )
    }

    componentWillUnmount() {
        this.willFocus();
    }

    alter = (oldPassword, newPassword, newPasswordC) => {

        if(oldPassword == '' || newPassword == '' || newPasswordC == '') {
            tudo.Alert.alert(
                'Atualizar perfil',
                'Tem de preencher todos os campos',
                [
                    { text: 'Ok', style: 'cancel' },
                ]
            )
        }else if (newPassword != newPasswordC) {
            tudo.Alert.alert(
                'Atualizar perfil',
                'As passwords inseridas não coincidem',
                [
                    { text: 'Ok', style: 'cancel' },
                ]
            )
        } else {
            var cred = auth.EmailAuthProvider.credential(this.state.email, oldPassword)
            user.reauthenticateWithCredential(cred)
                .then(() => {
                    tudo.Alert.alert(
                        'Atualizar perfil',
                        'Password atualizada com sucesso',
                        [
                            { text: 'Ok', onPress: () => this.goToMapPage() },
                        ]
                    )
                })
                .catch(error => {
                    tudo.Alert.alert(
                        'Atualizar perfil',
                        'Password antiga incorreta',
                        [
                            { text: 'Ok', style: 'cancel' },
                        ]
                    )
                })


        }
    }

    goToMapPage() {
        this.state.navigation.replace('MapDrawer', {
            screen: 'Map',
        });
    }


    render() {
        return (
            <tudo.View style={styles.full}>
                <tudo.View style={styles.imagePart}>
                    <tudo.Image
                        style={styles.imageP}
                        source={require('../images/logo.png')}
                    />
                    <tudo.Text style={styles.text}>Editar</tudo.Text>
                </tudo.View>


                <tudo.View style={styles.newPassword}>
                    <tudo.TextInput
                        style={styles.textinput} secureTextEntry={true}
                        placeholder='Password antiga'
                        onChangeText={(oldPassword) => this.setState({ oldPassword })}></tudo.TextInput>
                    <tudo.TextInput
                        style={styles.textinput} secureTextEntry={true}
                        placeholder='Nova password'
                        onChangeText={(newPassword) => this.setState({ newPassword })}></tudo.TextInput>
                    <tudo.TextInput
                        style={styles.textinput} secureTextEntry={true}
                        placeholder='Confirmar nova password'
                        onChangeText={(newPasswordC) => this.setState({ newPasswordC })}></tudo.TextInput>
                </tudo.View>

                <tudo.View style={styles.buttonview}>
                    <tudo.TouchableOpacity
                        style={styles.Logout}
                        onPress={() => this.alter(this.state.oldPassword, this.state.newPassword, this.state.newPasswordC)}>
                        <tudo.Text style={styles.imageButton}>Confirmar</tudo.Text>
                    </tudo.TouchableOpacity>
                </tudo.View>
            </tudo.View>
        )
    }
}


const styles = tudo.StyleSheet.create({
    full: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
    }, text: {
        color: 'black',
        fontSize: 25,
    },
    imagePart: {
        flex: 2.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    newPassword: {
        margin: 40,
        flex: 2.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    Logout: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 35,
        width: 130,
        backgroundColor: '#69dbd4',
        borderRadius: 5
    },
    textinput: {
        height: 40,
        width: 250,
        borderColor: 'gray',
        borderWidth: 1,
        margin: 10,
    },
    imageP: {
        marginTop: 40,
        marginBottom: 30,
        flex: 1,
        width: 112,
    },
    buttonview: {
        flex: 1,
        alignItems: 'center',
    },
})

export default Profile;