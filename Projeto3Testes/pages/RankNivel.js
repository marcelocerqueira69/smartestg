import React, { Component } from 'react';
import * as tudo from 'react-native';
import database from '@react-native-firebase/database';
import { firebase } from '@react-native-firebase/auth';

class RankNivel extends Component {
    constructor(props) {
        super(props);

        this.state = {
            navigation: props.navigation,
            listOfUsers: {},
            user: props.user.userLogged
        }
    }

    componentDidMount() {
        this.fetchUsers();
        
        this.props.setTitle('Ranking')

        this.willFocus = this.state.navigation.addListener(
            'focus', () => {
                this.props.setTitle('Ranking')
            }
        )

    }

    async fetchUsers() {
        await database()
            .ref('/users/')
            .orderByChild('nivel')
            .limitToLast(25)
            .once('value')
            .then((snapshot) => {
                var osUsers = [];
                var userLogged = this.state.user;
                snapshot.forEach(async function (childSnapshot) {
                    osUsers.push(childSnapshot.val());

                    if (childSnapshot.val().name == userLogged) {
                        if (childSnapshot.val().atributos.Rank == 0) {
                            await database()
                                .ref('/users/' + childSnapshot.val().name + '/atributos/')
                                .update({
                                    Rank: 1
                                })
                                .then(async () => {
                                    await database()
                                        .ref('/users/' + childSnapshot.val().name + '/medalhasGanhas/')
                                        .push(11)
                                        .then(console.log('Medalha de rank dada'))

                                    await database()
                                        .ref('/users/' + childSnapshot.val().name)
                                        .update({
                                            XP: database.ServerValue.increment(50)
                                        })
                                })
                        }
                    }

                })
                osUsers.reverse()
                this.setState({ listOfUsers: osUsers });

                var usersOfRank = osUsers.slice(0, 3);
                var i = 1;

                for (i; i <= usersOfRank.length; i++) {
                    if (usersOfRank[i - 1].name == this.state.user) {
                        switch (i) {
                            case 1:
                                if (usersOfRank[i - 1].atributos.FirstPlace == 0) {
                                    database()
                                        .ref('/users/' + this.state.user + '/atributos/')
                                        .update(({
                                            FirstPlace: database.ServerValue.increment(1)
                                        }))

                                    database()
                                        .ref('/users/' + this.state.user + '/medalhasGanhas/')
                                        .push(12)
                                        .then(console.log('u got first place badge'))

                                    database()
                                        .ref('/users/' + this.state.user)
                                        .update(({
                                            XP: database.ServerValue.increment(300)
                                        }))
                                }

                                break;
                            case 2:
                                if (usersOfRank[i - 1].atributos.SecondPlace == 0) {
                                    database()
                                        .ref('/users/' + this.state.user + '/atributos/')
                                        .update(({
                                            SecondPlace: database.ServerValue.increment(1)
                                        }))

                                    database()
                                        .ref('/users/' + this.state.user + '/medalhasGanhas/')
                                        .push(13)

                                    database()
                                        .ref('/users/' + this.state.user)
                                        .update(({
                                            XP: database.ServerValue.increment(200)
                                        }))
                                }
                                break;
                            case 3:
                                if (usersOfRank[i - 1].atributos.ThirdPlace == 0) {
                                    database()
                                        .ref('/users/' + this.state.user + '/atributos/')
                                        .update(({
                                            ThirdPlace: database.ServerValue.increment(1)
                                        }))

                                    database()
                                        .ref('/users/' + this.state.user + '/medalhasGanhas/')
                                        .push(14)

                                    database()
                                        .ref('/users/' + this.state.user)
                                        .update(({
                                            XP: database.ServerValue.increment(100)
                                        }))
                                }
                                break;
                            default:
                                console.log('not in podio zé');
                                break;
                        }
                    }
                }


            });
    }

    calculatePlace(index) {
        if (index == 0) {
            return (
                <tudo.Image style={styles.imagePlace} source={require('../images/1stPlace.png')}></tudo.Image>
            )
        } else if (index == 1) {
            return (
                <tudo.Image style={styles.imagePlace} source={require('../images/2ndPlace.png')}></tudo.Image>
            )
        } else if (index == 2) {
            return (
                <tudo.Image style={styles.imagePlace} source={require('../images/3rdPlace.png')}></tudo.Image>
            )
        } else {
            return (
                <tudo.Text style={styles.textForRank}>{index + 1}</tudo.Text>
            )
        }
    }

    render() {
        return (
            <tudo.View style={styles.full}>
                <tudo.FlatList style={styles.flatList}
                    data={this.state.listOfUsers}
                    keyExtractor={(item) => item.name}
                    renderItem={({ item, index }) => (
                        <tudo.View style={styles.flatView}>
                            <tudo.View style={styles.viewInRow}>
                                {this.calculatePlace(index)}
                            </tudo.View>
                            <tudo.View style={styles.viewInRow}>
                                <tudo.Text style={item.name == this.props.user.userLogged ? styles.textForUser : styles.text}>{item.name}</tudo.Text>
                            </tudo.View>
                            <tudo.View style={styles.viewInRow}>
                                <tudo.Text style={item.name == this.props.user.userLogged ? styles.textForUser : styles.text}>Nível {item.nivel}</tudo.Text>
                            </tudo.View>
                        </tudo.View>
                    )}
                />
            </tudo.View>
        )
    }
}


const styles = tudo.StyleSheet.create({
    full: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
    },
    imagePlace: {
        width: 40,
        height: 40,
    },
    flatView: {
        backgroundColor: "#ededed",
        padding: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#000000',
        flexDirection: 'row',
        alignItems: 'center',
    },
    viewInRow: {
        flex: 1,
    },
    text: {
        color: 'black',
        fontSize: 25,
        marginRight: 20,
    },
    textForUser: {
        color: '#9c0202',
        fontSize: 25,
        marginRight: 20,
    },
    nivel: {
        color: 'black',
        fontSize: 25,
        position: 'absolute',
    },
    textForRank: {
        color: 'black',
        fontSize: 40,
        marginLeft: 10,
    },
    flatList: {
        flex: 1,
        width: tudo.Dimensions.get('window').width
    }
})

export default RankNivel;