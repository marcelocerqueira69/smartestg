import React, { Component } from 'react';
import { View, StyleSheet, FlatList, SafeAreaView, ScrollView, SectionList, Image, Text, TouchableOpacity } from 'react-native';
import { ProgressBar } from '@react-native-community/progress-bar-android';
import Clipboard from "@react-native-clipboard/clipboard";
import database from '@react-native-firebase/database';
import firebase from '@react-native-firebase/app';
import storage from '@react-native-firebase/storage';


class MainProfile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            navigation: props.navigation,
            user: props.user.userLogged,
            userLvl: props.user.user.nivel,
            userExp: props.user.user.XP,
            inviterCode: props.user.user.inviterCode,
            indexMedalhasGanhas: [],
            medalhasGanhas: [],
            imagensMedalhas: [],
            medalhas: [],
            imagem: '',
        }
    }


    componentDidMount() {
        this.fetchData();

        this.props.setTitle('Perfil')

        this.willFocus = this.state.navigation.addListener(
            'focus', () => {
                this.props.setTitle('Perfil')
            }
        )
    }

    componentWillUnmount() {
        this.willFocus();
    }

    async getImages(imagens) {

        let i;
        var array = [];
        for (i = 0; i < imagens.length; i++) {
            array.push(await storage().ref().child(`${imagens[i].Img}`).getDownloadURL().then((url) => {
                return { ...imagens[i], url: url }
            }))
        }

        return array;
    }


    async fetchData() {

        await database()
            .ref('/users/' + this.state.user)
            .once('value')
            .then(snapshot => {
                if (snapshot.val().medalhasGanhas != undefined) {
                    this.setState({
                        indexMedalhasGanhas: Object.values(snapshot.val().medalhasGanhas)
                    })
                }
            })

        await database()
            .ref('/medalhas/')
            .once('value')
            .then(snapshot => {

                let medalhas = snapshot.val();
                medalhas.shift();
                this.setState({
                    medalhas: medalhas
                })

                let i;
                const array = [];
                for (i = 0; i < this.state.indexMedalhasGanhas.length; i++) {
                    array.push(this.state.medalhas[this.state.indexMedalhasGanhas[i] - 1])
                }

                this.setState({
                    medalhasGanhas: array
                })



            })
            .catch(err => console.log(err));
        this.setState({medalhasGanhas: await this.getImages(this.state.medalhasGanhas)});
    }

    goToMapPage() {
        this.state.navigation.replace('MapDrawer', {
            screen: 'Map',
        });
    }

    render() {
        return (
            <View style={styles.full}>
                <View style={styles.imagePart}>
                    <Text style={styles.username}>{this.state.user}</Text>
                </View>

                <View style={styles.progressView}>
                    <Text style={styles.text}>Nível {this.state.userLvl}</Text>
                    <ProgressBar styleAttr="Horizontal" indeterminate={false} progress={this.props.user.progress} style={styles.progressLevel}></ProgressBar>
                    <Text style={styles.text}>{this.state.userExp} XP</Text>
                </View>


                <SafeAreaView style={{ flex: 5 }}>
                    <FlatList
                        keyExtractor={(item, index) => index + ''}
                        style={{ marginTop: 20 }}
                        data={this.state.medalhasGanhas}
                        renderItem={({ item, index }) =>
                            <View style={{ flex: 1, width: 320, marginBottom: 15 }}>
                                <View style={{ flexDirection: 'row' }}>

                                    <Image style={{ resizeMode: 'contain', height: 70, width: 70 }} source={{ uri: item.url }}></Image>

                                    <Text style={{ fontSize: 15, marginTop: 22, marginLeft: 10, flex: 1 }}>{item.Nome}</Text>
                                </View>
                            </View>
                        }>

                    </FlatList>
                </SafeAreaView>


                <View style={styles.codeView}>
                    <Text>Código de Convite: {this.state.inviterCode}</Text>
                    <TouchableOpacity style={styles.copyBtn} onPress={Clipboard.setString(this.state.inviterCode)}>
                        <Text>Copiar</Text>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}


const styles = StyleSheet.create({
    full: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
    },
    username: {
        color: 'black',
        fontSize: 25,
    },
    imagePart: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageP: {
        marginTop: 40,
        marginBottom: 30,
        flex: 1,
        width: 112,
    },
    progressLevel: {
        width: 200
    },
    progressView: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        margin: 15
    },
    codeView: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center'
    },
    copyBtn: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 35,
        width: 130,
        backgroundColor: '#69dbd4',
        borderRadius: 5,
        margin: 10
    },
})

export default MainProfile;