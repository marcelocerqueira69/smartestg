import React, { Component } from 'react';
import * as tudo from 'react-native';
import database from '@react-native-firebase/database';

class RankConfs extends Component {
    constructor(props) {
        super(props);

        this.state = {
            navigation: props.navigation,
        }
    }


    render() {
        return (
            <tudo.View style={styles.full}>
                    <tudo.Text style={styles.text}>RankConfs will be here</tudo.Text>
            </tudo.View>
        )
    }
}


const styles = tudo.StyleSheet.create({
    full: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
    },
    flatView: {
        backgroundColor: "#dedede", 
        padding: 20, 
        borderBottomWidth: 1, 
        borderBottomColor: '#000000', 
        flexDirection: 'row'
    },
    text: {
        color: 'black',
        fontSize: 25,
    },
    imagePart: {
        flex: 2.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    newPassword: {
        margin: 40,
        flex: 2.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    Logout: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 35,
        width: 130,
        backgroundColor: '#69dbd4',
        borderRadius: 5
    },
    textinput: {
        height: 40,
        width: 250,
        borderColor: 'gray',
        borderWidth: 1,
        margin: 10,
    },
    imageP: {
        marginTop: 40,
        marginBottom: 30,
        flex: 1,
        width: 112,
    },
    buttonview: {
        flex: 1,
        alignItems: 'center',
    },
    FlatList: {
        flex: 1,
    }
})

export default RankConfs;