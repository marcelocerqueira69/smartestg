import React, { Component } from 'react';
import * as tudo from 'react-native';
import axios from 'axios';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            navigation: props.navigation,
            email: '',
            password: '',
            id: '',
            user: ''
        }
    }

    login = (email, password) => {
        if (email == '' && password == '') {
            tudo.Alert.alert(
                'Login',
                'Tem de inserir um email e uma password',
                [
                    { text: 'Ok', style: 'cancel' },
                ]
            )
        } else if (email != '' && password == '') {
            tudo.Alert.alert(
                'Login',
                'Tem de inserir uma password',
                [
                    { text: 'Ok', style: 'cancel' },
                ]
            )
        } else if (email == '' && password != '') {
            tudo.Alert.alert(
                'Login',
                'Tem de inserir um email',
                [
                    { text: 'Ok', style: 'cancel' },
                ]
            )
        } else {
            auth()
                .signInWithEmailAndPassword(email.trim(), password)
                .then((cenas) => {
                    database()
                        .ref('/users/' + cenas.user.displayName)
                        .once('value')
                        .then(snapshot => {
                            var XP, progress, nivel, nivelToXP;
                            nivel = snapshot.val().nivel
                            nivelToXP = nivel + 1;
                            XP = 100 * Math.pow(nivelToXP, 2)

                            if (snapshot.val().XP == 0) {
                                progress = 0;
                            } else {
                                progress = (XP / snapshot.val().XP) / 100;
                            }

                            this.props.setUser({ user: snapshot.val(), userLogged: snapshot.val().name, XPNedded: XP, progress: progress })
                        })
                    setTimeout(() => { this.goToMapPage() }, 500);
                })
                .catch(error => {
                    if (error.code === 'auth/wrong-password') {
                        tudo.Alert.alert(
                            'Login',
                            'Password errada',
                            [
                                { text: 'Ok', style: 'cancel' },
                            ]
                        )
                    }

                    if (error.code === 'auth/user-not-found') {
                        tudo.Alert.alert(
                            'Login',
                            'O email introduzido não existe',
                            [
                                { text: 'Ok', style: 'cancel' },
                            ]
                        )
                    }
                });
        }
    }

    goToMapPage() {
        this.state.navigation.replace('MapDrawer', {
            screen: 'Map',
        });
    }

    render() {
        return (
            <tudo.View style={styles.fullP}>
                <tudo.View style={styles.imagePart}>
                    <tudo.Image
                        style={styles.imageP}
                        source={require('../images/logo.png')}
                    />
                    <tudo.Text style={styles.text}>Login</tudo.Text>
                </tudo.View>

                <tudo.View style={styles.insertAndButton}>
                    <tudo.TextInput
                        style={styles.textinput}
                        placeholder='Email'
                        onChangeText={(email) => this.setState({ email })}>
                    </tudo.TextInput>
                    <tudo.TextInput
                        style={styles.textinput} secureTextEntry={true}
                        placeholder='Password'
                        onChangeText={(password) => this.setState({ password })}>
                    </tudo.TextInput>
                    <tudo.View style={styles.buttonview}>
                        <tudo.TouchableOpacity
                            style={styles.button}
                            onPress={() => this.login(this.state.email, this.state.password)}>
                            <tudo.Text style={styles.imageButton}>Login</tudo.Text>
                        </tudo.TouchableOpacity>
                    </tudo.View>

                    <tudo.Text
                        style={styles.underline}
                        onPress={() => this.props.navigation.navigate('Regist')}>Registar</tudo.Text>
                </tudo.View>
            </tudo.View>

        )
    }
}

const styles = tudo.StyleSheet.create({
    underline: {
        textDecorationLine: 'underline'
    },
    button: {
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center',
        height: 35,
        width: 130,
        backgroundColor: '#69dbd4',
        borderRadius: 5,
    },
    fullP: {
        flex: 1,
        flexDirection: 'column',
    },
    imagePart: {
        flex: 2.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    insertAndButton: {
        margin: 40,
        flex: 2.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageP: {
        marginTop: 40,
        flex: 1,
        width: 300,
    },
    buttonview: {
        flex: 1,
        alignItems: 'center',
    },
    text: {
        color: 'black',
        fontSize: 25,
    },
    textinput: {
        height: 40,
        width: 250,
        borderColor: 'gray',
        borderWidth: 1,
        margin: 10,
    },
})

export default Login;