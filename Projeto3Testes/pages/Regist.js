import React, { Component } from 'react';
import * as tudo from 'react-native';
import axios from 'axios';

import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';

class Regist extends Component {
    constructor(props) {
        super(props);

        this.state = {
            navigation: props.navigation,
            name: '',
            email: '',
            password: '',
            inviteCode: '',
            found: false,
            XP: 0,
            inviterName: '',
            foundName: false,
            convitesEnviados: 0,
        }
    }


    between = (min, max) => {
        return Math.floor(
            Math.random() * (max - min) + min
        )
    }

    validate = (email) => {
        const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

        return expression.test(String(email).toLowerCase())
    }

    regist = async (name, email, password, inviteCode) => {
        if (inviteCode != '') {
            await database()
                .ref('/users/')
                .once('value')
                .then(snapshot => {
                    snapshot.forEach((childSnap) => {
                        var data = childSnap.val();
                        if (data.inviterCode == this.state.inviteCode) {
                            this.setState({
                                found: true,
                                inviterName: data.name,
                            })
                        }
                    })
                })
        } else {
            this.state.found = true
        }

        await database()
            .ref('/users/' + name)
            .once('value')
            .then(snapshot => {
                if (snapshot.exists()) {
                    this.setState({ foundName: true })
                }
            })

        await database()
            .ref('/users/')
            .once('value')
            .then(snapshot => {
                this.state.count = snapshot.numChildren();
            })

        var number = this.between(100, 999);
        var inviterCode = `${number}${this.state.count}`


        if (this.validate(email) == false) {
            tudo.Alert.alert(
                'Registo',
                'Insira um email válido',
                [
                    { text: 'Ok', style: 'cancel' },
                ]
            )
        } else {
            if (name == '' || email == '' || password == '') {
                tudo.Alert.alert(
                    'Registo',
                    'Tem de inserir todos os campos',
                    [
                        { text: 'Ok', style: 'cancel' },
                    ]
                )
            } else if (this.state.found == false) {
                tudo.Alert.alert(
                    'Registo',
                    'Código de convite não existe. Insira um código de convite válido',
                    [
                        { text: 'Ok', style: 'cancel' },
                    ]
                )
            } else if (this.state.foundName == true) {
                tudo.Alert.alert(
                    'Registo',
                    'O username que introduziu já existe.',
                    [
                        { text: 'Ok', style: 'cancel' },
                    ]
                )
                this.setState({ foundName: false })
            } else {
                if (this.state.inviteCode == '') {
                    await auth()
                        .createUserWithEmailAndPassword(email, password)
                        .then(async () => {
                            await database()
                                .ref('/users/' + name)
                                .set({
                                    name: this.state.name,
                                    email: this.state.email,
                                    XP: 0,
                                    nivel: 0,
                                    inviterCode: inviterCode,
                                    atributos: {
                                        Confirmado: 0,
                                        Rank: 0,
                                        FirstPlace: 0,
                                        SecondPlace: 0,
                                        ThirdPlace: 0,
                                        Convidado: 0,
                                        ConvitesEnviados: 0,
                                    },
                                    medalhasGanhas: [],
                                    minutos: 0
                                })
                                .then(() => console.log('Data set to the invited.'));

                            await auth()
                                .currentUser
                                .updateProfile({
                                    displayName: name
                                })
                                .then(() => console.log('displayName set'))
                                .catch(err => console.log(err))

                            await database()
                                .ref('/users/' + name)
                                .once('value')
                                .then(snapshot => {
                                    var XP, progress, nivel, nivelToXP;
                                    nivel = snapshot.val().nivel
                                    nivelToXP = nivel + 1;
                                    XP = 100 * Math.pow(nivelToXP, 2)

                                    if (snapshot.val().XP == 0) {
                                        progress = 0;
                                    } else {
                                        progress = (XP / snapshot.val().XP) / 100;
                                    }

                                    this.props.setUser({ user: snapshot.val(), userLogged: snapshot.val().name, XPNedded: XP, progress: progress })
                                })

                            tudo.Alert.alert(
                                'Registo',
                                'Registo efetuado com sucesso',
                                [
                                    { text: 'Ok', onPress: () => this.goToLogin() },
                                ]
                            )
                        })
                        .catch(error => {
                            if (error.code === 'auth/email-already-in-use') {
                                tudo.Alert.alert(
                                    'Registo falhou',
                                    'O email que introduziu já existe',
                                    [
                                        { text: 'Ok', style: 'cancel' },
                                    ]
                                )
                            }
                            if (error.code === 'auth/weak-password') {
                                tudo.Alert.alert(
                                    'Registo falhou',
                                    'Password demasiado fraca. Deve conter, no mínimo, 6 caracteres.',
                                    [
                                        { text: 'Ok', style: 'cancel' },
                                    ]
                                )
                            }
                            console.error(error);
                        });


                } else {
                    await auth()
                        .createUserWithEmailAndPassword(email, password)
                        .then(async () => {
                            await database()
                                .ref('/users/' + name)
                                .set({
                                    name: this.state.name,
                                    email: this.state.email,
                                    XP: 0,
                                    nivel: 0,
                                    inviterCode: inviterCode,
                                    atributos: {
                                        Confirmado: 0,
                                        Rank: 0,
                                        FirstPlace: 0,
                                        SecondPlace: 0,
                                        ThirdPlace: 0,
                                        Convidado: 1,
                                        ConvitesEnviados: 0,
                                    },
                                    medalhasGanhas: [0, 7],
                                    minutos: 0
                                })
                                .then(() => console.log('Data set to the invited.'));

                            await database()
                                .ref('/users/' + this.state.inviterName)
                                .update({
                                    XP: database.ServerValue.increment(50),
                                })
                                .then(() => console.log('Data set to the inviter.'));

                            await database()
                                .ref('/users/' + this.state.inviterName + '/atributos/')
                                .update({
                                    ConvitesEnviados: database.ServerValue.increment(1),
                                })
                                .then(() =>
                                    console.log("Atributo Convites Enviados Adicionado")
                                );

                            await database()
                                .ref('/users/' + this.state.inviterName + '/atributos/ConvitesEnviados')
                                .once('value')
                                .then(snapshot => {

                                    let snap = snapshot.val();
                                    console.log(snap)
                                    this.setState({ convitesEnviados: snap })
                                    console.log(this.state.convitesEnviados)
                                })

                            switch (this.state.convitesEnviados) {
                                case 1:
                                    await database()
                                        .ref('/users/' + this.state.inviterName + '/medalhasGanhas')
                                        .push(1)
                                        .then(() =>
                                            console.log("Medalha 1 Convite added")
                                        );
                                    break;
                                case 5:
                                    await database()
                                        .ref('/users/' + this.state.inviterName + '/medalhasGanhas')
                                        .push(2)
                                        .then(() =>
                                            console.log("Medalha 5 Convites added")
                                        );
                                    break;
                                case 10:
                                    await database()
                                        .ref('/users/' + this.state.inviterName + '/medalhasGanhas')
                                        .push(3)
                                        .then(() =>
                                            console.log("Medalha 10 Convites added")
                                        );
                                    break;
                                default:
                                    console.log("Nenhuma medalha de convite added");
                                    break;
                            }

                            await auth()
                                .currentUser
                                .updateProfile({
                                    displayName: name
                                })
                                .then(() => console.log('displayName set'))
                                .catch(err => console.log(err))

                            await database()
                                .ref('/users/' + name)
                                .once('value')
                                .then(snapshot => {
                                    var XP, progress, nivel, nivelToXP;
                                    nivel = snapshot.val().nivel
                                    nivelToXP = nivel + 1;
                                    XP = 100 * Math.pow(nivelToXP, 2)

                                    if (snapshot.val().XP == 0) {
                                        progress = 0;
                                    } else {
                                        progress = (XP / snapshot.val().XP) / 100;
                                    }

                                    this.props.setUser({ user: snapshot.val(), userLogged: snapshot.val().name, XPNedded: XP, progress: progress })
                                })

                            tudo.Alert.alert(
                                'Registo',
                                'Registo efetuado com sucesso',
                                [
                                    { text: 'Ok', onPress: () => setTimeout(() => { this.goToLogin() }, 500) },
                                ]
                            )
                        })
                        .catch(error => {
                            if (error.code === 'auth/email-already-in-use') {
                                tudo.Alert.alert(
                                    'Registo falhou',
                                    'O email que introduziu já existe',
                                    [
                                        { text: 'Ok', style: 'cancel' },
                                    ]
                                )
                            }

                            if (error.code === 'auth/weak-password') {
                                tudo.Alert.alert(
                                    'Registo falhou',
                                    'Password demasiado fraca. Deve conter, no mínimo, 6 caracteres.',
                                    [
                                        { text: 'Ok', style: 'cancel' },
                                    ]
                                )
                            }

                            console.error(error);
                        });

                }
            }
        }
    }

    goToLogin() {
        this.state.navigation.replace('MapDrawer', {
            screen: 'Map',
        });
    }

    render() {
        return (
            <tudo.View style={styles.full}>
                <tudo.View style={styles.imagePart}>
                    <tudo.Image
                        style={styles.imageP}
                        source={require('../images/logo.png')}
                    />
                </tudo.View>
                <tudo.View style={styles.insertAndButton}>
                    <tudo.TextInput
                        style={styles.textinput}
                        placeholder='Nome'
                        onChangeText={(name) => this.setState({ name })}>
                    </tudo.TextInput>
                    <tudo.TextInput
                        style={styles.textinput}
                        placeholder='Email'
                        onChangeText={(email) => this.setState({ email })}>
                    </tudo.TextInput>
                    <tudo.TextInput
                        style={styles.textinput} secureTextEntry={true}
                        placeholder='Password'
                        onChangeText={(password) => this.setState({ password })}>
                    </tudo.TextInput>
                    <tudo.TextInput
                        style={styles.textinput}
                        placeholder='Código de convite (opcional)'
                        onChangeText={(inviteCode) => this.setState({ inviteCode })}>
                    </tudo.TextInput>
                    <tudo.TouchableOpacity
                        style={styles.button}
                        onPress={() => this.regist(this.state.name, this.state.email, this.state.password, this.state.inviteCode)}>
                        <tudo.Text style={styles.imageButton}>Registar</tudo.Text>
                    </tudo.TouchableOpacity>
                </tudo.View>
            </tudo.View>

        )
    }
}

const styles = tudo.StyleSheet.create({
    full: {
        flex: 1,
        flexDirection: 'column',
    },
    imagePart: {
        flex: 2.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    insertAndButton: {
        margin: 40,
        flex: 2.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageP: {
        marginTop: 40,
        flex: 1,
        width: 205,
    },
    text: {
        color: 'black',
        fontSize: 40
    },
    textinput: {
        height: 40,
        width: 250,
        borderColor: 'gray',
        borderWidth: 1,
        margin: 10,
    },
    buttonview: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    button: {
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center',
        height: 35,
        width: 130,
        backgroundColor: '#69dbd4',
        borderRadius: 5,
    },
})

export default Regist;