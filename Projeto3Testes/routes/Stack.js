import * as React from 'react';


import { NavigationContainer, DrawerActions, useRoute } from '@react-navigation/native';
import { TouchableOpacity, Image, StyleSheet, View, Avatar, Text, Dimensions, Alert, ButtonGroup, Button } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'; //q ueria por icons, couldnt do for now, sad
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import { ProgressBar } from '@react-native-community/progress-bar-android';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Dialog from 'react-native-dialog';


import Map from '../pages/Map';
import Login from '../pages/Login';
import Regist from '../pages/Regist';
import MainProfile from '../pages/MainProfile';
import RankConfs from '../pages/RankConfs';
import RankNivel from '../pages/RankNivel';
import Profile from '../pages/Profile';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createMaterialTopTabNavigator();

const { height: HEIGHT, width: WIDTH } = Dimensions.get('window');

function Rank(props) {
    const willFocus = props.navigation.addListener(
        'focus', () => {
            props.setTitle('Ranking')
        }
    )
    React.useEffect(() => {
        props.setTitle('Ranking')

        return () => {
            willFocus();
        }
    }, [])

    return (
        <Tab.Navigator {...props}>
            <Tab.Screen name="Ranking de Nível">
                {gervasio => <RankNivel {...props} />}
            </Tab.Screen>
            <Tab.Screen name="Ranking de Confirmações">
                {gervasio => <RankConfs {...props} />}
            </Tab.Screen>
        </Tab.Navigator>
    );
}

function CustomDrawerContent(props) {
    return (
        <DrawerContentScrollView {...props}>
            <View style={{ height: 200 }}>
                <Text style={{
                    color: '#69dbd4',
                    marginTop: '5%',
                    fontFamily: 'sans-serif-condensed',
                    alignSelf: 'center',
                    fontSize: 20
                }}>Olá {props.user.userLogged}</Text>
                <ProgressBar styleAttr="Horizontal" indeterminate={false} progress={props.user.progress} style={styles.progressLevel} />
                <Text style={{
                    color: '#69dbd4',
                    marginTop: '5%',
                    fontFamily: 'sans-serif-condensed',
                    alignSelf: 'center',
                    fontSize: 20
                }}>Nível {props.user.user.nivel}</Text>
                <Text style={{
                    color: '#69dbd4',
                    marginTop: '5%',
                    fontFamily: 'sans-serif-condensed',
                    alignSelf: 'center',
                    fontSize: 20
                }}>{props.user.user.XP}/{props.user.XPNedded} XP</Text>
            </View>
            <DrawerItemList {...props} />
            <DrawerItem
                style={{ marginTop: HEIGHT - 550 }}
                label="Logout"
                onPress={() => auth()
                    .signOut()
                    .then(() => props.navigation.replace('Login'))}
            />
        </DrawerContentScrollView>
    );
}



function Drawerr({ user, title, setTitle }) {
    const [fila, setFila] = React.useState()

    React.useEffect(() => {
        database()
            .ref('/filas/')
            .on('child_changed', snapshot => {
                setFila(snapshot.val())
            })
    })

    return (
        <Drawer.Navigator initialRouteName='Map' drawerContent={props => <CustomDrawerContent {...props} user={user} title={title} />}>
            <Drawer.Screen name="Map"
                options={{
                    title: 'Mapa',
                }} >
                {props => <Map {...props} user={user} title={title} setTitle={setTitle} fila={fila} setFila={setFila} />}
            </Drawer.Screen>
            <Drawer.Screen name="Rank"
                options={{
                    title: 'Ranking',
                }} >
                {props => <RankNivel {...props} user={user} title={title} setTitle={setTitle} />}
            </Drawer.Screen>
            <Drawer.Screen name="MainProfile" options={{
                title: 'Perfil',
            }}>
                {props => <MainProfile {...props} user={user} title={title} setTitle={setTitle} />}
            </Drawer.Screen>
            <Drawer.Screen name="Profile" options={{
                title: 'Editar Password',
            }}>
                {props => <Profile {...props} user={user} title={title} setTitle={setTitle} />}
            </Drawer.Screen>
        </Drawer.Navigator>
    )
}



function MainStack({ navigation }) {
    const [user, setUser] = React.useState({});
    const [title, setTitle] = React.useState('');

    const [initializing, setInitializing] = React.useState(true);
    const [userLoged, setUserLoged] = React.useState();

    const [visible, setVisible] = React.useState(false);
    const [radioSelectedData, setRadioSelectedData] = React.useState(0);
    const [active, setActive] = React.useState(-1)
    const radioData = [1, 3, 5, 10];

    const showDialog = () => {
        setVisible(true);
    }

    const handleCancel = () => {
        setRadioSelectedData(0)
        setActive(-1)
        setVisible(false);
    };

    const singleTapRadioSelectedButtons = (minute, index) => {
        setRadioSelectedData(minute)
        setActive(index)
    }

    const submitMinutes = async () => {
        if (radioSelectedData == 0) {
            Alert.alert(
                'Falha',
                'Tem de selecionar um valor. Se não estacionou, clique em "Cancelar".',
                [
                    { text: 'Ok' },
                ]
            )
        } else {

            await database()
                .ref('/users/' + user.userLogged)
                .update({
                    minutos: database.ServerValue.increment(radioSelectedData)
                })
                .then(() => {
                    setRadioSelectedData(0);
                    setActive(-1);
                    setVisible(false);
                });
        }
    }




    async function onAuthStateChanged(user) {
        setUserLoged(user);
        if (user != null) {
            await database()
                .ref('/users/' + user.displayName)
                .once('value')
                .then(snapshot => {
                    var XP, progress, nivel, nivelToXP;
                    nivel = snapshot.val().nivel
                    nivelToXP = nivel + 1;
                    XP = 100 * Math.pow(nivelToXP, 2)

                    if (snapshot.val().XP >= XP) {
                        var oXP = XP - snapshot.val().XP
                        if (oXP <= 0) {
                            database()
                                .ref('/users/' + user.displayName)
                                .update({
                                    nivel: database.ServerValue.increment(1),
                                    XP: -oXP
                                })
                                .then(snapshot => {
                                    nivel = snapshot.val().nivel
                                    switch (nivel) {
                                        case 5:
                                            database()
                                                .ref('/users/' + user.userLogged + '/medalhasGanhas/')
                                                .push(4)
                                                .then(console.log('Nivel 5'))

                                            database()
                                                .ref('/users/' + childSnapshot.val().name)
                                                .update({
                                                    XP: database.ServerValue.increment(10)
                                                })
                                            break;
                                        case 20:
                                            database()
                                                .ref('/users/' + user.userLogged + '/medalhasGanhas/')
                                                .push(5)
                                                .then(console.log('Nivel 20'))

                                            database()
                                                .ref('/users/' + childSnapshot.val().name)
                                                .update({
                                                    XP: database.ServerValue.increment(40)
                                                })
                                            break;
                                        case 50:
                                            database()
                                                .ref('/users/' + user.userLogged + '/medalhasGanhas/')
                                                .push(6)
                                                .then(console.log('Nivel 50'))

                                            database()
                                                .ref('/users/' + childSnapshot.val().name)
                                                .update({
                                                    XP: database.ServerValue.increment(100)
                                                })
                                            break;
                                        default:
                                            console.log('nível irrelevante');
                                            break;
                                    }
                                    nivelToXP = nivel + 1;
                                    XP = 100 * Math.pow(nivelToXP, 2)

                                    if (snapshot.val().XP == 0) {
                                        progress = 0;
                                    } else {
                                        progress = (XP / snapshot.val().XP);
                                    }

                                    setUser({ user: snapshot.val(), userLogged: snapshot.val().name, XPNedded: XP, progress: progress })
                                })
                                .catch(err => console.log(err))
                        }
                    } else {
                        if (snapshot.val().XP == 0) {
                            progress = 0;
                        } else {
                            progress = (snapshot.val().XP / XP);
                        }
                        setUser({ user: snapshot.val(), userLogged: snapshot.val().name, XPNedded: XP, progress: progress })

                    }


                })
                .catch(error => {
                    console.log(error)
                })
        }
        if (initializing) setInitializing(false);
    }

    React.useEffect(() => {
        if (user.userLogged != undefined) {
            database()
                .ref('/users/' + user.userLogged)
                .on('value', snapshot => {

                    var XP, progress, nivel, nivelToXP;
                    nivel = snapshot.val().nivel
                    nivelToXP = nivel + 1;
                    XP = 100 * Math.pow(nivelToXP, 2)

                    if (snapshot.val().XP >= XP) {
                        var oXP = XP - snapshot.val().XP
                        if (oXP <= 0) {
                            database()
                                .ref('/users/' + user.userLogged)
                                .update({
                                    nivel: database.ServerValue.increment(1),
                                    XP: -oXP
                                })
                                .then(snapshot => {
                                    nivel = snapshot.val().nivel
                                    switch (nivel) {
                                        case 5:
                                            database()
                                                .ref('/users/' + user.userLogged + '/medalhasGanhas/')
                                                .push(4)
                                                .then(console.log('Nivel 5'))

                                            database()
                                                .ref('/users/' + user.userLogged)
                                                .update({
                                                    XP: database.ServerValue.increment(10)
                                                })
                                            break;
                                        case 20:
                                            database()
                                                .ref('/users/' + user.userLogged + '/medalhasGanhas/')
                                                .push(5)
                                                .then(console.log('Nivel 20'))

                                            database()
                                                .ref('/users/' + user.userLogged)
                                                .update({
                                                    XP: database.ServerValue.increment(40)
                                                })
                                            break;
                                        case 50:
                                            database()
                                                .ref('/users/' + user.userLogged + '/medalhasGanhas/')
                                                .push(6)
                                                .then(console.log('Nivel 50'))

                                            database()
                                                .ref('/users/' + user.userLogged)
                                                .update({
                                                    XP: database.ServerValue.increment(100)
                                                })
                                            break;
                                        default:
                                            console.log('nível irrelevante');
                                            break;
                                    }
                                    nivelToXP = nivel + 1;
                                    XP = 100 * Math.pow(nivelToXP, 2)

                                    if (snapshot.val().XP == 0) {
                                        progress = 0;
                                    } else {
                                        progress = (XP / snapshot.val().XP);
                                    }

                                    setUser({ user: snapshot.val(), userLogged: snapshot.val().name, XPNedded: XP, progress: progress })
                                })
                                .catch(err => console.log(err))
                        }
                    } else {
                        if (snapshot.val().XP == 0) {
                            progress = 0;
                        } else {
                            progress = (XP / snapshot.val().XP);
                        }

                        setUser({ user: snapshot.val(), userLogged: snapshot.val().name, XPNedded: XP, progress: progress })
                    }
                });

        }
        const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
        return subscriber;
    }, [radioSelectedData, active]);

    if (initializing) return null;

    if (!userLoged) {
        return (
            <NavigationContainer>
                <Stack.Navigator initialRouteName='Login'>
                    <Stack.Screen name='Login'
                        options={{
                            headerShown: false,
                            headerStyle: { backgroundColor: '#69dbd4' },
                        }} >
                        {props => <Login {...props} setUser={setUser} />}
                    </Stack.Screen>
                    <Stack.Screen name='Regist'
                        options={{
                            headerStyle: { backgroundColor: '#69dbd4' },
                            title: 'Registo'
                        }} >
                        {props => <Regist {...props} setUser={setUser} />}
                    </Stack.Screen>
                    <Stack.Screen name='Profile'
                        options={{
                            headerStyle: { backgroundColor: '#69dbd4' },
                            title: 'Editar Perfil'
                        }} >
                        {props => <Profile {...props} setUser={setUser} />}
                    </Stack.Screen>
                    <Stack.Screen name='MapDrawer'
                        options={({ navigation }) => ({
                            headerStyle: { backgroundColor: '#69dbd4' },
                            title: title,
                            headerLeft: () => (
                                <TouchableOpacity
                                    style={styles.Touch}
                                    onPress={() => navigation.dispatch(DrawerActions.openDrawer())}>
                                    <Image style={styles.imageButton} source={require('../images/open-menu.png')} />
                                </TouchableOpacity>
                            ),
                            headerRight: () => (
                                <TouchableOpacity
                                    style={styles.Touch2}
                                    onPress={() => showDialog()}>
                                    <Text>
                                        Estacionómetro
                                    </Text>
                                </TouchableOpacity>
                            ),
                        })} >
                        {props => <View style={styles.viewDoDrawer}>
                            <Drawerr {...props} user={user} setTitle={setTitle} />
                            <Dialog.Container visible={visible} style={{ width: WIDTH - 100 }}>
                                <Dialog.Title>Quantos minutos poupou a estacionar?</Dialog.Title>
                                <Dialog.Description>
                                    <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
                                        {radioData.map((minute, index) => (
                                            <View key={index} style={{ margin: (WIDTH - 260) / 8 }}>
                                                <TouchableOpacity
                                                    style={index == active ? styles.activeTouchableButton : styles.touchableButton}
                                                    onPress={() => singleTapRadioSelectedButtons(minute, index)}>
                                                    <Text style={{ color: 'black' }}>{minute}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        ))}
                                    </View>
                                </Dialog.Description>
                                <Dialog.Button label="Cancelar" onPress={handleCancel} />
                                <Dialog.Button label="Submeter" onPress={submitMinutes} />
                            </Dialog.Container>
                        </View>}
                    </Stack.Screen>
                </Stack.Navigator>
            </NavigationContainer>
        )
    } else {
        return (
            <NavigationContainer>
                <Stack.Navigator initialRouteName='MapDrawer'>
                    <Stack.Screen name='Login'
                        options={{
                            headerShown: false,
                            headerStyle: { backgroundColor: '#69dbd4' },
                        }} >
                        {props => <Login {...props} setUser={setUser} />}
                    </Stack.Screen>
                    <Stack.Screen name='Regist'
                        options={{
                            headerStyle: { backgroundColor: '#69dbd4' },
                            title: 'Registo'
                        }} >
                        {props => <Regist {...props} setUser={setUser} />}
                    </Stack.Screen>
                    <Stack.Screen name='MapDrawer'
                        options={({ navigation }) => ({
                            headerStyle: { backgroundColor: '#69dbd4' },
                            title: title,
                            headerLeft: () => (
                                <TouchableOpacity
                                    style={styles.Touch}
                                    onPress={() => navigation.dispatch(DrawerActions.openDrawer())}>
                                    <Image style={styles.imageButton} source={require('../images/open-menu.png')} />
                                </TouchableOpacity>
                            ),
                            headerRight: () => (
                                <TouchableOpacity
                                    style={styles.Touch2}
                                    onPress={() => showDialog()}>
                                    <Text>
                                        Estacionómetro
                                    </Text>
                                </TouchableOpacity>
                            ),
                        })} >
                        {props => (<View style={styles.viewDoDrawer}>
                            <Dialog.Container visible={visible} style={{ width: WIDTH - 100 }}>
                                <Dialog.Title>Quantos minutos poupou a estacionar?</Dialog.Title>
                                <Dialog.Description>
                                    <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
                                        {radioData.map((minute, index) => (
                                            <View key={index} style={{ margin: (WIDTH - 260) / 8 }}>
                                                <TouchableOpacity
                                                    style={index == active ? styles.activeTouchableButton : styles.touchableButton}
                                                    onPress={() => singleTapRadioSelectedButtons(minute, index)}>
                                                    <Text style={{ color: 'black' }}>{minute}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        ))}
                                    </View>
                                </Dialog.Description>
                                <Dialog.Button label="Cancelar" onPress={handleCancel} />
                                <Dialog.Button label="Submeter" onPress={submitMinutes} />
                            </Dialog.Container>
                            <Drawerr {...props} user={user} setTitle={setTitle} />
                        </View>)}
                    </Stack.Screen>
                </Stack.Navigator>
            </NavigationContainer>
        )

    }


}

export default MainStack;

const styles = StyleSheet.create({
    Touch: {
        marginLeft: 20
    },
    Touch2: {
        marginRight: 20,
        height: 30,
        width: 120,
        borderRadius: 10,
        backgroundColor: '#60b5b0',
        borderColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1
    },
    viewDoDrawer: {
        height: '100%',
        width: '100%'
    },
    imageButton: {
        width: 20,
        height: 20,
    },
    activeTouchableButton: {
        marginRight: 5,
        width: 40,
        height: 40,
        backgroundColor: '#416966',
        borderRadius: 10,
        alignItems: 'center',
        borderColor: 'black',
        borderWidth: 1,
    },
    touchableButton: {
        marginRight: 5,
        width: 40,
        height: 40,
        backgroundColor: '#69dbd4',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'black',
        borderWidth: 1,
    },
    progressLevel: {
        width: '95%',
        marginLeft: '2.5%'
    },
})