import 'react-native-gesture-handler';
import React, { Component } from 'react';

import Navigator from './routes/Stack';

const App = () => {
  return (
    <Navigator/>
  );
};

export default App;
